import config
import os
import spacy
from textacy.extract import pos_regex_matches
from sklearn.utils import shuffle
from collections import Counter


NUMERICAL_VALUE_PATTERNS = list()
NUMERICAL_VALUE_PATTERNS.append('<NUM> <NOUN> (<ADP> <NOUN>)?')
NUMERICAL_VALUE_PATTERNS.append('<NUM> <AD> <NOUN>')
nlp = spacy.load('fr', disable=['ner'])
def extract_numerical_values(sentence):
    return [span.text.lower() for span in \
            pos_regex_matches(nlp(sentence), ' | '.join(NUMERICAL_VALUE_PATTERNS))]


all_stats_entities = set()
for keyphrase_file in config.KEYPHRASE_FILES:
    all_stats_entities.update(open(keyphrase_file).read().splitlines())

def find_stats_entities(sentence):
    sentence = sentence.lower()
    terms = list()
    for term in all_stats_entities:
        if term in sentence:
            terms.append(term)
    return terms


def extract_candidates():
    with open(config.CANDIDATE_SENTENCES) as f:
        next(f)

        unique_entities = set()
        lines = f.read().splitlines()
        indices = range(len(lines))
        indices, lines = shuffle(indices, lines, random_state=2000)
        for _id, line in zip(indices, lines):
            # Retrieve the sentence content
            first_comma_index = line.index(',')
            json_file_id = line[:first_comma_index]
            second_comma_index = line.index(',', first_comma_index + 1)
            published_date = line[first_comma_index + 1: second_comma_index].strip()
            sentence = line[second_comma_index + 1:].strip()

            # Search for only 1 date time component
            heidel_out_file = os.path.join(config.HEIDEL_OUTPUT_FOLDER, 'out_%d' % _id)
            if os.path.exists(heidel_out_file):
                dates = open(heidel_out_file).read().splitlines()
                if len(dates) == 1:
                    _type, date = dates[0].split(', ')
                    if _type == 'DATE':
                        # The date time component could be considered as 
                        # a numerical value too. So remove it before 
                        # extracting numerical values
                        numerical_values = extract_numerical_values(\
                                sentence.replace(date, ' '))
                        if len(numerical_values) == 1:
                            stats_entities = find_stats_entities(sentence)
                            if len(stats_entities) == 1:
                                entity = stats_entities[0]
                                if entity not in unique_entities:
                                    unique_entities.add(entity)

                                    print(sentence)
                                    # print(published_date)
                                    print('%s %s' % (entity, date))
                                    print(numerical_values[0])
                                    print('-' * 20)

                                    if len(unique_entities) >= 200:
                                        break


def find_frequent_terms(output_file):
    with open(config.CANDIDATE_SENTENCES) as f:
        next(f)

        counter = Counter()
        lines = f.read().splitlines()
        indices = range(len(lines))
        indices, lines = shuffle(indices, lines, random_state=2000)
        for _id, line in zip(indices, lines):
            # Retrieve the sentence content
            first_comma_index = line.index(',')
            json_file_id = line[:first_comma_index]
            second_comma_index = line.index(',', first_comma_index + 1)
            published_date = line[first_comma_index + 1: second_comma_index].strip()
            sentence = line[second_comma_index + 1:].strip()

            # Search for only 1 date time component
            heidel_out_file = os.path.join(config.HEIDEL_OUTPUT_FOLDER, 'out_%d' % _id)
            if os.path.exists(heidel_out_file):
                dates = open(heidel_out_file).read().splitlines()
                if len(dates) == 1:
                    _type, date = dates[0].split(', ')
                    if _type == 'DATE':
                        # The date time component could be considered as 
                        # a numerical value too. So remove it before 
                        # extracting numerical values
                        numerical_values = extract_numerical_values(\
                                sentence.replace(date, ' '))
                        for entity in find_stats_entities(sentence):
                            counter[entity] += 1

        with open(output_file, 'w+') as f:
            for term, freq in counter.most_common():
                f.write('%s, %d\n' % (term, freq))


if __name__ == '__main__':
    extract_candidates()
    # find_frequent_terms('frequent_terms_news.txt')
