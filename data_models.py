import hashlib


class TableMetadata(object):
    def __init__(self, title='', description='', comment='',
                 header_rows=dict(), header_columns=dict(),
                 insee_id=None, file_path=None, link=None):
        '''
        Args:
            header_rows {header_id: header_value}
            header_columns {header_id: header_value}
        '''
        self.title = title
        self.description = description
        self.comment = comment
        self.header_rows = header_rows
        self.header_columns = header_columns
        self.insee_id = insee_id
        self.file_path = file_path
        self.link = link


class DataSet(object):
    def __init__(self, url, downloaded_date,
                 downloaded_file_path, description):
        ''' Represents a file downloaded from INSEE

        Args:
            url (str):
            downloaded_date (str):
            downloaded_file_path (str):
            description (str):
        '''
        self.url = url
        self.downloaded_date = downloaded_date
        self.downloaded_file_path = downloaded_file_path
        self.description = description

    def dataset_id(self):
        return hashlib.sha224(self.downloaded_file_path.encode('utf-8')).hexdigest()


class Sheet(object):
    def __init__(self, _id=0,
                 title='', comment='',
                 ttl_file_path='', published_date=''):
        ''' Represents a sheet from a `DataSet` object

        Args:
           _id (int): position of this `Sheet` in `DataSet`, starting from 0
           title (str):
           comment (str):
           ttl_file_path (str): The .ttl file that generated from this `Sheet`
           published_date (str)
        '''
        self._id = _id
        self.title = title
        self.comment = comment
        self.ttl_file_path = ttl_file_path
        self.published_date = published_date

    def sheet_id(self):
        return hashlib.sha224(self.ttl_file_path.encode('utf-8')).hexdigest()
