CANDIDATE_SENTENCES = 'candidate_sentences.csv'
'''
CSV file that contains candidate sentences in format (JSON file ID, 
published date, candidate sentence).
All JSON files are stored at /data/tcao/news-please-repo/data/
'''

HEIDEL_OUTPUT_FOLDER = '/data/tcao/heidel_out/'
'''
The folder where the extracted date time is stored.
Sentence at position k in CANDIDATE_SENTENCES has the extracted date time stored at 
file "HEIDEL_OUTPUT_FOLDER + out_k" under format:
type1, value1
type2, value2
'''

KEYPHRASE_FILES = [
    # 'filtered_key_phrases.txt',
    # 'terms_title.txt',
    # 'terms_header.txt',
    # 'terms_comment.txt'
    'keywords_kd.txt',
    'keywords_unit.txt'
]
